package com.codeondev.indianfreejobs.provider;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class Category {

    private String ID, title, desc, count;

    public Category() {
    }

    public Category(String ID, String title, String desc, String count) {
        this.ID = ID;
        this.title = title;
        this.desc = desc;
        this.count = count;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPostCount() {
        return count;
    }

    public void setPostCount(String count) {
        this.count = count;
    }
}
