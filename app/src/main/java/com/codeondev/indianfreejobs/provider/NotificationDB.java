package com.codeondev.indianfreejobs.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.codeondev.indianfreejobs.database.DBAdapter;
import com.codeondev.indianfreejobs.util.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikhil Jain on 25-Dec-15.
 */
public class NotificationDB extends DBAdapter{

    private static final String KEY_ID = "_id";
    private static final String KEY_TITLE = "noti_title";
    private static final String KEY_MESSAGE = "noti_msg";
    private static final String KEY_READ_STATUS = "noti_read_status";
    private static final String KEY_IMG_URL = "noti_img_url";
    private static final String KEY_DTTM = "noti_dttm";

    private int id, status;
    private String title, msg, img, dttm;

    public NotificationDB(){

    }

    public NotificationDB(String title, String msg, int status, String img, String dttm){
        this.title = title;
        this.msg = msg;
        this.status = status;
        this.img = img;
        this.dttm = dttm;
    }

    public void setID(int ID){
        this.id = ID;
    }

    public int getID(){
        return this.id;
    }


    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }


    public void setMsg(String msg){
        this.msg = msg;
    }

    public String getMsg(){
        return this.msg;
    }


    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return this.status;
    }


    public void setImg(String img){
        this.img = img;
    }

    public String getImg(){
        return this.img;
    }


    public void setDttm(String dttm){
        this.dttm = dttm;
    }

    public String getDttm(){
        return this.dttm;
    }


    public static void addData(NotificationDB notification) {
        final SQLiteDatabase db = open();
        ContentValues cVal = new ContentValues();
        cVal.put(KEY_TITLE, notification.getTitle());
        cVal.put(KEY_MESSAGE, notification.getMsg());
        cVal.put(KEY_READ_STATUS, notification.getStatus());
        cVal.put(KEY_IMG_URL, notification.getImg());
        cVal.put(KEY_DTTM, notification.getDttm());
        db.insert(Constant.NOTIFICATION_TBL, null, cVal);
        db.close();
    }

    public static void addDataList(List<NotificationDB> user) {
        final SQLiteDatabase db = open();
        ContentValues cVal = new ContentValues();
        for(int i=0; i<user.size(); i++){
            cVal.put(KEY_TITLE, user.get(i).getTitle());
            cVal.put(KEY_MESSAGE, user.get(i).getMsg());
            cVal.put(KEY_READ_STATUS, user.get(i).getStatus());
            cVal.put(KEY_IMG_URL, user.get(i).getImg());
            cVal.put(KEY_DTTM, user.get(i).getDttm());
            db.insert(Constant.NOTIFICATION_TBL, null, cVal);
        }
        db.close();
    }

    public static int updateReadStatus(NotificationDB data) {
        final SQLiteDatabase db = open();
        ContentValues values = new ContentValues();
        values.put(KEY_READ_STATUS, data.getStatus());

        return db.update(Constant.NOTIFICATION_TBL, values, KEY_ID + " = ?",
                new String[] { String.valueOf(data.getID()) });
    }

    public static NotificationDB getNotificationData(int id) {
        final SQLiteDatabase db = open();
        Cursor cursor = db.query(Constant.NOTIFICATION_TBL, new String[] { KEY_ID,
                        KEY_TITLE, KEY_MESSAGE, KEY_READ_STATUS,
                        KEY_IMG_URL, KEY_DTTM }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            return new NotificationDB(cursor.getString(1), cursor.getString(2),
                    cursor.getInt(3), cursor.getString(4),
                    cursor.getString(5));
        }
        else
            return null;
    }

    public static List<NotificationDB> getAllData() {
        List<NotificationDB> list = new ArrayList<>();

        final SQLiteDatabase db = open();

        String selectQuery = "SELECT  * FROM " + Constant.NOTIFICATION_TBL + " ORDER BY "
                 + KEY_DTTM + " DESC";
        Cursor cursor = db.rawQuery ( selectQuery, null );

        if (cursor.moveToFirst()) {
            do {
                NotificationDB data = new NotificationDB();
                data.setID(Integer.parseInt(cursor.getString(0)));
                data.setTitle(cursor.getString(1));
                data.setMsg(cursor.getString(2));
                data.setStatus(cursor.getInt(3));
                data.setImg(cursor.getString(4));
                data.setDttm(cursor.getString(5));
                list.add(data);
            } while (cursor.moveToNext());
        }
        return list;
    }

    public static void deleteData(NotificationDB data) {
        final SQLiteDatabase db = open();
        if(getNotificationData(data.getID()) != null){
            db.delete(Constant.NOTIFICATION_TBL, KEY_ID + " = ?",
                    new String[]{String.valueOf(data.getID())});
        }
        db.close();
    }

    public static int getUserDataCount() {
        final SQLiteDatabase db = open();
        String countQuery = "SELECT  * FROM " + Constant.NOTIFICATION_TBL;
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public static void deleteAllData() {
        final SQLiteDatabase db = open();
        db.delete(Constant.NOTIFICATION_TBL, null, null);
        db.close();
    }
}
