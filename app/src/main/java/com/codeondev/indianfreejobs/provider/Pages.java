package com.codeondev.indianfreejobs.provider;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class Pages {
    private int ID;
    private String title, status;

    public Pages() {
    }

    public Pages(int ID, String title, String status) {
        this.ID = ID;
        this.title = title;
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
