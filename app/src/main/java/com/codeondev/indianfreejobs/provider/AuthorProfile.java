package com.codeondev.indianfreejobs.provider;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class AuthorProfile {

    private int ID;
    private String name, email, image, usr_nm;

    public AuthorProfile() {
    }

    public AuthorProfile(int ID, String name, String email, String usr_nm, String image) {
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.image = image;
        this.usr_nm = usr_nm;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsrNm() {
        return usr_nm;
    }

    public void setUsrNm(String usr_nm) {
        this.image = usr_nm;
    }
}
