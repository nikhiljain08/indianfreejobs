package com.codeondev.indianfreejobs.provider;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class Search {
    private int ID;
    private String title, guid, cmt_count;

    public Search() {
    }

    public Search(int ID, String title, String guid, String cmt_count) {
        this.ID = ID;
        this.title = title;
        this.guid = guid;
        this.cmt_count = cmt_count;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCmtCount() {
        return cmt_count;
    }

    public void setCmtCount(String cmt_count) {
        this.cmt_count = cmt_count;
    }
}
