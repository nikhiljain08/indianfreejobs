package com.codeondev.indianfreejobs.provider;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class RecentPost {

    private String title, img_url, post_dttm, guid, cmt_count, views;
    private int id;

    public RecentPost() {
    }

    public RecentPost(int id, String title, String img_url, String post_dttm,
                      String guid, String cmt_count, String views) {
        this.id = id;
        this.title = title;
        this.img_url = img_url;
        this.post_dttm = post_dttm;
        this.guid = guid;
        this.cmt_count = cmt_count;
        this.views = views;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getImage() {
        return img_url;
    }

    public void setImage(String img_url) {
        this.img_url = img_url;
    }


    public String getPostDate() {
        return post_dttm;
    }

    public void setPostDate(String post_dttm) {
        this.post_dttm = post_dttm;
    }


    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCmtCount() {
        return cmt_count;
    }

    public void setCmtCount(String cmt_count) {
        this.cmt_count = cmt_count;
    }

    public String getViewCount() {
        return views;
    }

    public void setViewCount(String views) {
        this.views = views;
    }
}
