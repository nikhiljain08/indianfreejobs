package com.codeondev.indianfreejobs.provider;

import java.util.Comparator;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class Comment {

    private String id, name, email, img_url, post_dttm, comment_txt;

    public Comment() {
    }

    public Comment(String id, String name, String email, String comment_txt,
                   String img_url, String post_dttm) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.comment_txt = comment_txt;
        this.img_url = img_url;
        this.post_dttm = post_dttm;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getImage() {
        return img_url;
    }

    public void setImage(String img_url) {
        this.img_url = img_url;
    }


    public String getPostDate() {
        return post_dttm;
    }

    public void setPostDate(String post_dttm) {
        this.post_dttm = post_dttm;
    }


    public String getComment() {
        return comment_txt;
    }

    public void setComment(String comment_txt) {
        this.comment_txt = comment_txt;
    }

    public static Comparator<Comment> COMPARE_BY_DATE = new Comparator<Comment>() {
        public int compare(Comment o1, Comment o2) {
            return o2.post_dttm.compareTo(o1.post_dttm);
        }
    };
}
