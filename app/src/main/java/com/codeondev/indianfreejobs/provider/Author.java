package com.codeondev.indianfreejobs.provider;

import java.util.List;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class Author {

    private int ID;
    private String name, email, image, post_cnt;

    public Author() {
    }

    public Author(int ID, String name, String email, String image, String post_cnt) {
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.image = image;
        this.post_cnt = post_cnt;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String role) {
        this.image = role;
    }

    public String getPCount() {
        return post_cnt;
    }

    public void setPCount(String post_cnt) {
        this.post_cnt = post_cnt;
    }
}
