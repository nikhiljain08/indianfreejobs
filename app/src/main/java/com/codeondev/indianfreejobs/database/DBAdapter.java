package com.codeondev.indianfreejobs.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.Constant;


/**
 * Created by Nikhil Jain on 20-June-16.
 */
public class DBAdapter {

    private static final String DATABASE_NAME = R.string.app_name + "_db.db";
    private static final int DATABASE_VERSION = 1;

    private static final String[] ALL_TABLES = {Constant.NOTIFICATION_TBL};
    private static final String ALL_TABLES_CREATE[] = {Constant.NOTIFICATION_TBL_CREATE} ;

    private static DataBaseHelper DBHelper = null;

    public void init(Context context) {
        if (DBHelper == null) {
            DBHelper = new DataBaseHelper(context);
        }
    }

    private static class DataBaseHelper extends SQLiteOpenHelper {
        private DataBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            for (String table : ALL_TABLES_CREATE) {
                db.execSQL(table);
                Log.e("DB", table);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            for (String table : ALL_TABLES) {
                db.execSQL("DROP TABLE IF EXISTS " + table);
            }
            onCreate(db);
        }

    }

    public static synchronized SQLiteDatabase open() throws SQLException {
        return DBHelper.getWritableDatabase();
    }
}
