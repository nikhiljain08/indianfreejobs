package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.NotificationDB;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>  {

    private final GlobalClass gc;
    private ArrayList<NotificationDB> notificationArrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, msg, dttm;
        public LinearLayout dot;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            msg = (TextView) view.findViewById(R.id.msg);
            dttm = (TextView) view.findViewById(R.id.dttm);
            dot = (LinearLayout) view.findViewById(R.id.noti_dot);
        }
    }


    public NotificationAdapter(Context context, ArrayList<NotificationDB> notificationArrayList) {
        this.notificationArrayList = notificationArrayList;
        this.context = context;
        gc = new GlobalClass(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationDB notification = notificationArrayList.get(position);
        holder.title.setText(notification.getTitle());
        holder.msg.setText(notification.getMsg());
        holder.dttm.setText(gc.getlongtoago(notification.getDttm()));

        if(notification.getStatus() == 1) {
            holder.dot.setVisibility(View.INVISIBLE);
        } else if(notification.getStatus() == 0) {
            holder.dot.setVisibility(View.VISIBLE);
        }

        gc.setFont(holder.title);
        gc.setFont(holder.msg);
        gc.setFont(holder.dttm);
    }

    @Override
    public int getItemCount() {
        return notificationArrayList.size();
    }
}
