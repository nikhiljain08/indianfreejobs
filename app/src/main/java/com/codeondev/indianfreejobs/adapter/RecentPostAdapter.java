package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class RecentPostAdapter extends RecyclerView.Adapter<RecentPostAdapter.MyViewHolder>  {

    private final Context context;
    private final GlobalClass gc;
    private ArrayList<RecentPost> recentPostList;
    private int lastPosition = -1;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, cmt_cnt;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.recent_post_title);
            //content = (TextView) view.findViewById(R.id.recent_post_content);
            image = (ImageView) view.findViewById(R.id.recent_post_image);
            cmt_cnt = (TextView) view.findViewById(R.id.recent_comments);
        }
    }


    public RecentPostAdapter(Context context, ArrayList<RecentPost> recentPostList) {
        this.context = context;
        this.recentPostList = recentPostList;
        gc = new GlobalClass(context);
    }


    @Override
    public RecentPostAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recentpost, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecentPostAdapter.MyViewHolder holder, int position) {
        RecentPost recentPost = recentPostList.get(position);
        holder.title.setText(recentPost.getTitle());

        String post_footer = recentPost.getViewCount()
                + " views  \u25CF  " +
                recentPost.getCmtCount()
                + " comments  \u25CF  " + gc.getlongtoago(recentPost.getPostDate());
        holder.cmt_cnt.setText(post_footer);

        if(!recentPost.getImage().equals("false")) {
            holder.image.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(recentPost.getImage())
                    //.centerCrop()
                    //.fitCenter()
                    //.thumbnail(0.2f)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(holder.image);
        }

        gc.setFont(holder.title);
        gc.setFont(holder.cmt_cnt);
        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return recentPostList.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
