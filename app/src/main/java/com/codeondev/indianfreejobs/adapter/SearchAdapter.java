package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.Pages;
import com.codeondev.indianfreejobs.provider.Search;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder>  {

    private final GlobalClass gc;
    private ArrayList<Search> searchList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
        }
    }


    public SearchAdapter(Context context, ArrayList<Search> searchList) {
        this.searchList = searchList;
        gc = new GlobalClass(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Search search = searchList.get(position);
        holder.title.setText(search.getTitle());
        gc.setFont(holder.title);
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }
}
