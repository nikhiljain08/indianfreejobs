package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.helper.ColorGenerator;
import com.codeondev.indianfreejobs.helper.TextDrawable;
import com.codeondev.indianfreejobs.provider.Category;
import com.codeondev.indianfreejobs.provider.Pages;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class PagesAdapter extends RecyclerView.Adapter<PagesAdapter.MyViewHolder>  {

    private final GlobalClass gc;
    private final Typeface typeface;
    private ArrayList<Pages> pagesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView title_initial;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            title_initial = (ImageView) view.findViewById(R.id.title_initial);
        }
    }


    public PagesAdapter(Context context, ArrayList<Pages> pagesList) {
        this.pagesList = pagesList;
        gc = new GlobalClass(context);
        typeface = Typeface.createFromAsset(context.getAssets(), Constant.FONT_NAME_R);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pages, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pages pages = pagesList.get(position);

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color1 = generator.getRandomColor();

        if(pages.getStatus().equals("publish")) {
            holder.title.setText(pages.getTitle());
            TextDrawable drawable = TextDrawable
                    .builder()
                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(typeface)
                    .fontSize(50)
                    .toUpperCase()
                    .endConfig()
                    .buildRound(pages.getTitle().substring(0,1), color1);
            holder.title_initial.setImageDrawable(drawable);
        }

        gc.setFont(holder.title);
    }

    @Override
    public int getItemCount() {
        return pagesList.size();
    }
}
