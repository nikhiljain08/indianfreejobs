package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.Author;
import com.codeondev.indianfreejobs.provider.Category;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class AuthorAdapter extends RecyclerView.Adapter<AuthorAdapter.MyViewHolder>  {

    private final GlobalClass gc;
    private final Context context;
    private ArrayList<Author> authorList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, email;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            image = (ImageView) view.findViewById(R.id.author_image);
        }
    }


    public AuthorAdapter(Context context, ArrayList<Author> authorList) {
        this.authorList = authorList;
        this.context = context;
        gc = new GlobalClass(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.author, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Author author = authorList.get(position);

        holder.name.setText(author.getName());
        holder.email.setText(author.getEmail());

        Glide.with(context)
                .load(author.getImage())
                .asBitmap()
                .thumbnail(0.2f)
                .placeholder(R.drawable.user)
                .centerCrop().into(new BitmapImageViewTarget(holder.image) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                holder.image.setImageDrawable(circularBitmapDrawable);
            }
        });

        gc.setFont(holder.email);
        gc.setFont(holder.name);
    }

    @Override
    public int getItemCount() {
        return authorList.size();
    }
}
