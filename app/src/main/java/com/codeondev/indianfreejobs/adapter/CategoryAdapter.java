package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codeondev.indianfreejobs.provider.Category;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder>  {

    private final GlobalClass gc;
    private ArrayList<Category> categoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, count;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            count = (TextView) view.findViewById(R.id.count);
        }
    }


    public CategoryAdapter(Context context, ArrayList<Category> categoryList) {
        this.categoryList = categoryList;
        gc = new GlobalClass(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Category category = categoryList.get(position);
        holder.title.setText(gc.htmlStringParser(category.getTitle()));

        if(!category.getDesc().equals("")){
            holder.desc.setVisibility(View.VISIBLE);
            holder.desc.setText(category.getDesc());
        }
        holder.count.setText(category.getPostCount());

        gc.setFont(holder.title);
        gc.setFont(holder.desc);
        gc.setFont(holder.count);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
