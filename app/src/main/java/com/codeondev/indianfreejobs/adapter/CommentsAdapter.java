package com.codeondev.indianfreejobs.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.Comment;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder>  {

    private final Context context;
    private final GlobalClass gc;
    private ArrayList<Comment> commentList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, dttm, cmt;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.comment_name);
            image = (ImageView) view.findViewById(R.id.comment_image);
            dttm = (TextView) view.findViewById(R.id.comment_dttm);
            cmt = (TextView) view.findViewById(R.id.comment_txt);
        }
    }


    public CommentsAdapter(Context context, ArrayList<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
        gc = new GlobalClass(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Comment comment = commentList.get(position);

        holder.name.setText(comment.getName());
        holder.dttm.setText(gc.getlongtoago(comment.getPostDate()) + " ago");
        holder.cmt.setText(comment.getComment());

        Glide.with(context)
                .load(comment.getImage())
                .asBitmap()
                .thumbnail(0.2f)
                .placeholder(R.drawable.user)
                .centerCrop().into(new BitmapImageViewTarget(holder.image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.image.setImageDrawable(circularBitmapDrawable);
                    }
                });


        gc.setFont(holder.name);
        gc.setFont(holder.dttm);
        gc.setFont(holder.cmt);
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }
}
