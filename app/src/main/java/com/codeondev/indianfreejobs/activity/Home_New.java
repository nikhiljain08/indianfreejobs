package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.RecentPostFragment;
import com.codeondev.indianfreejobs.Top10Fragment;
import com.codeondev.indianfreejobs.TrendingFragment;
import com.codeondev.indianfreejobs.database.DBAdapter;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.DeviceInfo;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

public class Home_New extends AppCompatActivity
        implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private String TAG = "HOME";
    private GlobalClass gc;
    private ViewPager mViewPager;

    private MainPagerAdapter mainPagerAdapter;
    private int MAIN_NUM_ITEMS = 3;
    private long back_pressed;
    private PrefManager prefManager;
    private View header;
    private BottomNavigationView bnve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        DBAdapter db = new DBAdapter();
        db.init(this);

        prefManager = new PrefManager(this);

        final String token = FirebaseInstanceId.getInstance().getToken();
        //Log.e(TAG, token);

        if (!prefManager.getFCMToken().equals(token) && token != null) {
            if(gc.checkNetworkStatus()) {
                updateFCMtoken(token);
            }
        }

        bnve = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        mViewPager = (ViewPager) findViewById(R.id.main_viewpager);

        mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mainPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bnve.setSelectedItemId(R.id.action_recent);
                        break;
                    case 1:
                        bnve.setSelectedItemId(R.id.action_trending);
                        break;
                    case 2:
                        bnve.setSelectedItemId(R.id.action_top10);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        bnve.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_recent:
                                mViewPager.setCurrentItem(0,true);
                                break;
                            case R.id.action_trending:
                                mViewPager.setCurrentItem(1,true);
                                break;
                            case R.id.action_top10:
                                mViewPager.setCurrentItem(2,true);
                                break;
                        }
                        return true;
                    }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header = navigationView.getHeaderView(0);
        header.findViewById(R.id.profile_header_layout).setOnClickListener(this);

        Menu m = navigationView.getMenu();
        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(),
                Constant.FONT_NAME_R);

        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SpannableString s1 = new SpannableString(mi.getTitle());
            s1.setSpan(new TypefaceSpan(typeface), 0, s1.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mi.setTitle(s1);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    SpannableString s = new SpannableString(subMenuItem.getTitle());
                    s.setSpan(new TypefaceSpan(typeface), 0, s.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    subMenuItem.setTitle(s);
                }
            }
        }

    }

    private void updateFCMtoken(final String token) {
        final DeviceInfo info = new DeviceInfo(getApplicationContext());

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.FCM_TOKEN_REG);

        String url = gc.urlCreator(hashMap);
        Log.e(TAG, url);
        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        if (response!= null) {
                            prefManager.setFCMToken(token);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Home_New.this,
                                "Some error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constant.DEVICE_ID, info.getAndroidID());
                params.put(Constant.DEVICE_TYPE, "Android");
                params.put(Constant.FCM_TOKEN, token);
                if(prefManager.userEmail().equals(""))
                    params.put("email", prefManager.userEmail());
                params.put("subscribe", "global");

                return params;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsObjRequest,TAG);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //handler.removeCallbacks(runnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //AppController.getInstance().cancelPendingRequests(TAG);
    }

    public void updateProfile(){
        if (header instanceof ViewGroup)
            gc.setFont(header);

        if(prefManager.isUserLogged()){

            header.findViewById(R.id.slider_name).setVisibility(View.VISIBLE);
            ((TextView) header.findViewById(R.id.slider_name)).setText(prefManager.userName());

            header.findViewById(R.id.slider_email).setVisibility(View.VISIBLE);;
            ((TextView) header.findViewById(R.id.slider_email)).setText(prefManager.userEmail());

            final ImageView img = (ImageView) header.findViewById(R.id.slider_image);
            img.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).
                    load(prefManager.userProfile())
                    .asBitmap()
                    .placeholder(R.drawable.user)
                    .centerCrop().into(new BitmapImageViewTarget(img) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img.setImageDrawable(circularBitmapDrawable);
                }
            });
        } else {
            header.findViewById(R.id.no_login).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()){
                super.onBackPressed();
            }
            else{
                Snackbar.make(findViewById(R.id.coordinatelayout),
                        "Press once again to exit!", Snackbar.LENGTH_LONG).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            startActivity(new Intent(Home_New.this, SearchViewActivity.class));
        } else if (id == R.id.action_notification) {
            startActivity(new Intent(Home_New.this, NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_category) {
            startActivity(new Intent(this, CategoryActivity.class));
        } else if (id == R.id.nav_author) {
            startActivity(new Intent(this, AuthorActivity.class));
        } else if (id == R.id.nav_post) {

        } else if (id == R.id.nav_page) {
            startActivity(new Intent(this, PagesActivity.class));

        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutUs.class));
        } else if (id == R.id.nav_contact) {

        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_header_layout:
                if(!prefManager.isUserLogged())
                    startActivity(new Intent(Home_New.this, LoginActivity.class));
                break;
        }
    }

    private class MainPagerAdapter extends FragmentPagerAdapter {

        private MainPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return MAIN_NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return RecentPostFragment.newInstance();
                case 1:
                    return TrendingFragment.newInstance();
                case 2:
                    return Top10Fragment.newInstance();
                default:
                    return null;
            }
        }
    }


    private class TypefaceSpan extends MetricAffectingSpan {
        private final Typeface typeface;

        public TypefaceSpan(Typeface typeface) {
            this.typeface = typeface;
        }

        private void apply(Paint paint, Typeface typeface) {
            paint.setTypeface(typeface);
        }

        @Override
        public void updateDrawState(TextPaint textPaint) {
            apply(textPaint, typeface);
        }

        @Override
        public void updateMeasureState(TextPaint textPaint) {
            apply(textPaint, typeface);
        }
    }


}
