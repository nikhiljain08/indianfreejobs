package com.codeondev.indianfreejobs.activity;

import android.annotation.SuppressLint;
import android.app.MediaRouteButton;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.provider.Author;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikhil Jain on 07-Jun-17.
 */

public class PostActivity extends AppCompatActivity implements View.OnClickListener{
    private String TAG = "POST";
    private GlobalClass gc;
    private WebView content;
    private String mapTitle, mapContent, guid;
    private String cmt_count;
    private String id;
    private FloatingActionButton share_btn;
    private String mapImage;
    private ImageView header_img;
    boolean fav = false;
    private int mapPostView;
    private TextView post_title;
    private ProgressDialog progressDialog;
    private String mapAuthor_name, mapAuthor_image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recentpost_new);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        id = String.valueOf(bundle.getInt("id"));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_post);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressDialog = new ProgressDialog(PostActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        findViewById(R.id.add_comments).setOnClickListener(this);
        findViewById(R.id.fav_layout).setOnClickListener(this);

        content = (WebView) findViewById(R.id.post_webview);
        share_btn = (FloatingActionButton) findViewById(R.id.share_btn);
        header_img = (ImageView) findViewById(R.id.post_image);
        post_title = (TextView) findViewById(R.id.recent_post_title);

        share_btn.setOnClickListener(this);

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.POST_URL);
        hashMap.put(Constant.ID, String.valueOf(bundle.getInt("id")));

        String url = gc.urlCreator(hashMap);
        //String url = Constant.MAIN_URL + Constant.POST_URL;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONObject>() {

            @SuppressLint("SetJavaScriptEnabled")
            @SuppressWarnings("unchecked")
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                if(response.toString() != null)
                    updatePost(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PostActivity.this, "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });

        jsObjRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    public void updatePost(JSONObject response){
        try {
            mapTitle = response.getString("post_title");
            mapContent = response.getString("post_content");
            mapImage = response.getString("image");
            mapPostView = response.getInt("post_view");
            cmt_count = response.getString("comment_count");
            guid = response.getString("guid");

            JSONObject author = response.getJSONObject("author");
            mapAuthor_name = author.getString("display_name");
            mapAuthor_image = author.getString("image");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        post_title.setText(mapTitle);

        Glide.with(getApplicationContext())
                .load(mapImage)
                .centerCrop()
                .fitCenter()
                .placeholder(R.mipmap.ic_launcher)
                .into(header_img);

        ((TextView) findViewById(R.id.comments_count)).setText(cmt_count);
        ((TextView) findViewById(R.id.views_count)).setText(String.valueOf(mapPostView));

        ((TextView) findViewById(R.id.author_name)).setText(mapAuthor_name);
        final ImageView auth_image = (ImageView) findViewById(R.id.author_img);
        Glide.with(getApplicationContext())
                .load(mapAuthor_image)
                .asBitmap()
                .placeholder(R.drawable.user)
                .centerCrop().into(new BitmapImageViewTarget(auth_image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        auth_image.setImageDrawable(circularBitmapDrawable);
                    }
                });


        content.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                //mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url){
                progressDialog.dismiss();
            }

        });

//                    content.setWebChromeClient(new WebChromeClient(){
//                        public void onProgressChanged(WebView view, int newProgress){
//                            mProgressBar.setProgress(newProgress);
//                            if(newProgress == 100){
//                                mProgressBar.setVisibility(View.GONE);
//                            }
//                        }
//                    });

        content.getSettings().setAppCacheEnabled(true);
        content.getSettings().setJavaScriptEnabled(true);

        content.loadDataWithBaseURL(null,
                "<style>img{display: inline;height: auto;max-width: 100%;}</style>"
                        + StringEscapeUtils.unescapeHtml4(mapContent),"text/html","UTF-8", null);


    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comments:
                Intent intent = new Intent(PostActivity.this, CommentsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("post_id", id);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.fav_layout:
                updateFav();
                break;
            case R.id.share_btn:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, guid);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }
    }

    private void updateFav(){
        ImageView imageView = (ImageView) findViewById(R.id.fav_image);
        if(fav){
            fav = false;
            imageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_border_white_18dp));
        } else {
            fav = true;
            imageView.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_white_18dp));
        }
    }
}
