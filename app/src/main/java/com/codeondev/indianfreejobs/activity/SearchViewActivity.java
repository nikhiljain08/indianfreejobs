package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.SearchAdapter;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.Search;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nikhil on 26/6/17.
 */

public class SearchViewActivity extends AppCompatActivity {
    private GlobalClass gc;
    private AVLoadingIndicatorView avi;
    private EditText search_str;
    private ImageView close_search;
    private String TAG = "SearchView";
    private String search_query;
    private SearchAdapter myAdapter;
    private ArrayList<Search> myDataSet = new ArrayList<>();;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchview_activity);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
        search_str = (EditText) findViewById(R.id.search_str);
        close_search = (ImageView) findViewById(R.id.close_search);

        stopAnim();
        close_search.setVisibility(View.INVISIBLE);

        close_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search_str.getText().clear();
                myDataSet.clear();
                myAdapter.notifyDataSetChanged();
            }
        });

        findViewById(R.id.search_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        search_str.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                search_query = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() > 2) {
                    myDataSet.clear();
                    myAdapter.notifyDataSetChanged();
                    startAnim();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run(){
                            search(search_query);
                        }
                    }, 500);
                } else{
                    myDataSet.clear();
                    myAdapter.notifyDataSetChanged();
                }
            }
        });

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));

        myAdapter = new SearchAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.searchview_list);
        myRecyclerView.addItemDecoration(divider);
        myRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Search search = myDataSet.get(position);
                Intent intent = new Intent(SearchViewActivity.this, PostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", search.getID());
                bundle.putString("title", search.getTitle());
                bundle.putString("guid", search.getGuid());
                bundle.putString("count", search.getCmtCount());
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        myRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                gc.hideKeyboard(recyclerView);
            }
        });
    }


    public void search(String query){
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.SEARCH_URL);
        hashMap.put(Constant.QUERY, query);

        String url = gc.urlCreator(hashMap);

        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, response.toString());
                if(response.toString() != null)
                    showResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SearchViewActivity.this, "Check your internet!", Toast.LENGTH_SHORT).show();
                stopAnim();
            }
        });

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setTag(TAG);
        AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    private void showResult(JSONArray response) {
        stopAnim();
        for (int i=0; i< response.length(); i++) {
            JSONObject search;
            try {
                search = response.getJSONObject(i);
                myDataSet.add(new Search(search.getInt("ID"),
                        search.getString("post_title"),
                        search.getString("guid"),
                        search.getString("comment_count")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    void startAnim(){
        //avi.show();
        close_search.setVisibility(View.INVISIBLE);
        avi.smoothToShow();
    }

    void stopAnim(){
        close_search.setVisibility(View.VISIBLE);
        avi.hide();
        //avi.smoothToHide();
    }
}
