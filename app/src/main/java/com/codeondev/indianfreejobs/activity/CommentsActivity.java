package com.codeondev.indianfreejobs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.ImageSliderFragment;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.CommentsAdapter;
import com.codeondev.indianfreejobs.provider.Comment;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by nikhil on 24/6/17.
 */

public class CommentsActivity extends AppCompatActivity implements View.OnClickListener{
    private GlobalClass gc;
    private String post_id;
    private String TAG = "CommentsActivity";
    private CommentsAdapter myAdapter;
    private ArrayList<Comment> myDataSet = new ArrayList<>();
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PrefManager prefManager;
    private EditText comment_txt;
    private ProgressBar progBar;
    private ImageView add_img;
    private LinearLayout comment_sec;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prefManager = new PrefManager(this);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        post_id = bundle.getString("post_id");

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));

        myAdapter = new CommentsAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.comment_listview);
        myRecyclerView.addItemDecoration(divider);
        myRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                    getComments();
                }
            }
        );

        findViewById(R.id.add_comment_btn).setOnClickListener(this);
        add_img = (ImageView) findViewById(R.id.add_img);
        comment_sec = (LinearLayout) findViewById(R.id.comment_section);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getComments();
    }

    public void getComments() {
        RequestQueue queue = Volley.newRequestQueue(this);

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.POST_URL);
        hashMap.put(Constant.ID, post_id);

        String url = gc.urlCreator(hashMap);
        //String url = Constant.MAIN_URL + Constant.POST_URL;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONObject>() {

            @SuppressWarnings("unchecked")
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                if(response.toString() != null){
                    listComments(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommentsActivity.this,
                        "Some error occurred", Toast.LENGTH_SHORT).show();
            }
        });

        jsObjRequest.setShouldCache(false);
        queue.add(jsObjRequest);
    }

    private void listComments(JSONObject response) {
        myDataSet.clear();
        JSONArray cmt_array = new JSONArray();
        try {
            cmt_array = response.getJSONArray("comments");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i=0; i< cmt_array.length(); i++) {
            JSONObject comment;
            try {
                comment = cmt_array.getJSONObject(i);
                myDataSet.add(new Comment(comment.getString("comment_ID"),
                        comment.getString("comment_author"),
                        comment.getString("comment_author_email"),
                        gc.htmlStringParser(comment.getString("comment_content")),
                        comment.getString("comment_karma"),
                        comment.getString("comment_date")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(myDataSet, Comment.COMPARE_BY_DATE);

        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public static void enableDisableView(View view, boolean enabled) {
        view.setEnabled(enabled);
        if ( view instanceof ViewGroup ) {
            ViewGroup group = (ViewGroup)view;

            for ( int idx = 0 ; idx < group.getChildCount() ; idx++ ) {
                enableDisableView(group.getChildAt(idx), enabled);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.add_comment_btn:
                addComment();
                Log.e(TAG, "times");
                break;
        }
    }

    private void addComment() {
        comment_txt = (EditText) findViewById(R.id.comment_edittext);
        final String comment_str = comment_txt.getText().toString().trim();

        if(comment_str.length() > 0) {
            if (prefManager.isUserLogged()) {

                add_img.setVisibility(View.INVISIBLE);
                enableDisableView(comment_sec, false);
                progBar = (ProgressBar) findViewById(R.id.comment_pb);
                if (progBar.getIndeterminateDrawable() != null) {
                    progBar.getIndeterminateDrawable().setColorFilter(getApplicationContext()
                                    .getResources().getColor(android.R.color.white),
                            PorterDuff.Mode.SRC_IN);
                }
                progBar.setVisibility(View.VISIBLE);

                gc.hideKeyboard(findViewById(android.R.id.content));

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(Constant.CONNECT_URL, Constant.ADD_COMMENT_URL);

                String url = gc.urlCreator(hashMap);
                //String url = Constant.MAIN_URL + Constant.POST_URL;

                StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @SuppressWarnings("unchecked")
                            @Override
                            public void onResponse(String response) {
                                Log.e(TAG, response);
                                if (response!= null) {
                                    updateListComment(response);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(CommentsActivity.this,
                                        "Some error occurred!", Toast.LENGTH_SHORT).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<>();
                        params.put("comment_post_ID", post_id);
                        params.put("comment_author_email", prefManager.userEmail());
                        params.put("comment_author", prefManager.userName());
                        params.put("comment_content", comment_str);

                        return params;
                    }
//
//                    @Override
//                    public Map<String, String> getHeaders() throws AuthFailureError {
//                        Map<String,String> params = new HashMap<>();
//                        params.put("Content-Type","application/x-www-form-urlencoded");
//                        return params;
//                    }
                };

                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setShouldCache(false);
                AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
            } else {
                //progressDialog.dismiss();
                Toast.makeText(this, "Please login to comment!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    public void updateListComment(String response){
        try {
            //String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime());
            JSONObject jsonObject = new JSONObject(response);
            myDataSet.add(new Comment(jsonObject.getString("comment_post_ID"),
                    jsonObject.getString("comment_author"),
                    jsonObject.getString("comment_author_email"),
                    gc.htmlStringParser(jsonObject.getString("comment_content")),
                    prefManager.userProfile(),
                    jsonObject.getString("comment_date")));

            Collections.sort(myDataSet, Comment.COMPARE_BY_DATE);
            comment_txt.getText().clear();
            myRecyclerView.setAdapter(myAdapter);
            myAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        add_img.setVisibility(View.VISIBLE);
        progBar.setVisibility(View.GONE);
        enableDisableView(comment_sec, true);
    }
}
