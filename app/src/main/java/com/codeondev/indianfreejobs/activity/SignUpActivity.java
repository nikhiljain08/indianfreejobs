package com.codeondev.indianfreejobs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nikhil on 4/7/17.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private GlobalClass gc;
    private PrefManager prefManager;
    private EditText user_fname, user_lname, user_name, user_email, user_rpass, user_pass;
    private ProgressDialog progressDialog;
    private String TAG = "Sign Up Activity";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        prefManager = new PrefManager(this);

        user_fname = (EditText) findViewById(R.id.user_fname);
        user_lname = (EditText) findViewById(R.id.user_lname);
        user_name = (EditText) findViewById(R.id.user_name);
        user_email = (EditText) findViewById(R.id.user_email);
        user_pass = (EditText) findViewById(R.id.user_pass);
        user_rpass = (EditText) findViewById(R.id.user_repass);
        init();

        findViewById(R.id.signup_btn).setOnClickListener(this);
        findViewById(R.id.activity_close).setOnClickListener(this);
    }

    public void init() {
        Drawable fnameDrawable = user_fname.getBackground();
        Drawable lnameDrawable = user_lname.getBackground();
        Drawable unameDrawable = user_name.getBackground();
        Drawable emailDrawable = user_email.getBackground();
        Drawable passDrawable = user_pass.getBackground();
        Drawable repassDrawable = user_rpass.getBackground();

        fnameDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        lnameDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        unameDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        emailDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        passDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        repassDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        if(Build.VERSION.SDK_INT > 16) {
            user_fname.setBackground(fnameDrawable);
            user_lname.setBackground(lnameDrawable);
            user_name.setBackground(unameDrawable);
            user_email.setBackground(emailDrawable);
            user_pass.setBackground(passDrawable);
            user_rpass.setBackground(repassDrawable);
        } else {
            user_fname.setBackgroundDrawable(fnameDrawable);
            user_lname.setBackgroundDrawable(lnameDrawable);
            user_name.setBackgroundDrawable(unameDrawable);
            user_email.setBackgroundDrawable(emailDrawable);
            user_pass.setBackgroundDrawable(passDrawable);
            user_rpass.setBackgroundDrawable(repassDrawable);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup_btn:
                signUp();
                break;
            case R.id.activity_close:
                signUpFinish();
                break;
        }
    }

    private void signUp() {

        final String fname = user_fname.getText().toString().trim();
        final String lname = user_lname.getText().toString().trim();
        final String name = user_name.getText().toString().trim();
        final String email = user_email.getText().toString().trim();
        final String pass = user_pass.getText().toString().trim();
        final String rpass = user_rpass.getText().toString().trim();

        if (fname.length() > 0) {
            if (lname.length() > 0) {
                if (name.length() > 0) {
                    if (email.length() > 0) {
                        if (gc.emailValidator(email)) {
                            if (gc.passValidator(pass)) {
                                if (rpass.length() > 0) {
                                    if (pass.equals(rpass)) {
                                        signUpMain(fname, lname, name, email, pass);
                                    } else {
                                        Toast.makeText(this, "Confirm password mismatch!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(this, "Please enter confirm password!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(this, "Please enter alteast 6 characters!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, "Please enter valid Email address!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, "Please enter Email Address!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Please enter Username!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please Last Name!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please First Name!", Toast.LENGTH_SHORT).show();
        }
    }

    public void signUpMain(final String fname, final String lname,
                           final String name, final String email, final String pass){

        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Signing Up..");
        progressDialog.show();

        gc.hideKeyboard(findViewById(android.R.id.content));

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.SIGNUP_URL);

        String url = gc.urlCreator(hashMap);

        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        if (response!= null) {
                            updateSignUpStatus(response);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(SignUpActivity.this,
                                "Some error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_fname", fname);
                params.put("user_lname", lname);
                params.put("user_login", name);
                params.put("user_email", email);
                params.put("user_pass", pass);
                return params;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
    }

    private void updateSignUpStatus(String response) {
        JSONObject user;
        try {
            user = new JSONObject(response);
            if(user.getString("success").equals("true")){
                JSONObject data = user.getJSONObject("data");
                String name = data.getString("display_name");
                String email = data.getString("user_email");
                //String personPhoto = ((user.getPhotoUrl() != null) ? user.getPhotoUrl().toString() : null);

                prefManager.setUserLoggedIn(true, name, email, null, null);
                progressDialog.dismiss();
                Toast.makeText(this, "Welcome " + name + "!", Toast.LENGTH_SHORT).show();
                signUpFinish();
            } else {
                JSONObject error = user.getJSONObject("Error");
                JSONObject errors = error.getJSONObject("errors");
                progressDialog.dismiss();
                Toast.makeText(this, errors.getString("existing_user_login"), Toast.LENGTH_SHORT).show();
                //Toast.makeText(this, errors.getString("existing_user_email"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void signUpFinish(){
        Intent intent = new Intent(SignUpActivity.this, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

}
