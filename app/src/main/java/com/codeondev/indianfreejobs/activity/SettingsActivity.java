package com.codeondev.indianfreejobs.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;

/**
 * Created by nikhil on 13/7/17.
 */

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{
    private GlobalClass gc;
    private PrefManager prefManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        prefManager = new PrefManager(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(prefManager.isNotificationOn())
            ((Switch) findViewById(R.id.notification_switch)).setChecked(true);
        else
            ((Switch) findViewById(R.id.notification_switch)).setChecked(false);

        findViewById(R.id.notification_switch).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.notification_switch:
                if(!prefManager.isNotificationOn()) {
                    ((Switch) findViewById(R.id.notification_switch)).setChecked(true);
                    prefManager.setNotification(true);
                }
                else {
                    ((Switch) findViewById(R.id.notification_switch)).setChecked(false);
                    prefManager.setNotification(false);
                }
                break;
        }
    }
}
