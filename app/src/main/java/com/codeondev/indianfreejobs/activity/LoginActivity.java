package com.codeondev.indianfreejobs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nikhil on 21/6/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener {
    
    private CallbackManager callbackManager;
    private String TAG = "Login Activity";
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 1;
    private int RC_FB_IN = 64206;
    private GlobalClass gc;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.setBackgroundColor(getApplication().getResources().getColor(android.R.color.white));
//        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

//        assert getSupportActionBar() != null;
//        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_close_black_24dp);
//        getSupportActionBar().setElevation(0);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        prefManager = new PrefManager(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this , this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        EditText user_name = (EditText) findViewById(R.id.user_email);
        EditText user_pass = (EditText) findViewById(R.id.user_pass);
        LinearLayout login_btn = (LinearLayout) findViewById(R.id.login_btn);

        Drawable emailDrawable = user_name.getBackground();
        Drawable passDrawable = user_pass.getBackground();
        emailDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        passDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        if(Build.VERSION.SDK_INT > 16) {
            user_name.setBackground(emailDrawable);
            user_pass.setBackground(passDrawable);
        } else {
            user_name.setBackgroundDrawable(emailDrawable);
            user_pass.setBackgroundDrawable(passDrawable);
        }

        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.login_button).setOnClickListener(this);

        GradientDrawable g_drawable = (GradientDrawable) findViewById(R.id.sign_in_button).getBackground();
        GradientDrawable fb_drawable = (GradientDrawable) findViewById(R.id.login_button).getBackground();

        g_drawable.setColor(getApplicationContext().getResources().getColor(R.color.google));
        fb_drawable.setColor(getApplicationContext().getResources().getColor(R.color.com_facebook_button_background_color));

        callbackManager = CallbackManager.Factory.create();

        findViewById(R.id.activity_close).setOnClickListener(this);
        findViewById(R.id.skip).setOnClickListener(this);
        findViewById(R.id.login_btn).setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
    }

    public void loginStatus(){
        Intent intent = new Intent(LoginActivity.this, Home_New.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(prefManager.isUserLogged())
            loginStatus();
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        private ProgressDialog progressDialog;

        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d(TAG, "facebook:onSuccess:" + loginResult);

            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Processing");
            progressDialog.show();

            final String accessToken = loginResult.getAccessToken().getToken();
            Log.i("accessToken", accessToken);

            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                Log.i("LoginActivity", response.toString());
                try {
                    String id = object.getString("id");
                    URL profile_pic = null;
                    String name = null;
                    String email = null;
                    try {
                        profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=200");
                        name = object.getString("first_name") + " " + object.getString("last_name");
                        email = object.getString("email");
                        Log.e(TAG,profile_pic+name+email);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                    assert profile_pic != null;
                    prefManager.setUserLoggedIn(true, name, email, accessToken, profile_pic.toString());
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Welcome back " + name + "!",
                            Toast.LENGTH_SHORT).show();
                    loginStatus();
                } catch (JSONException e) {
                    Log.d(TAG, "Error parsing JSON");
                }
                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            Log.d(TAG, "facebook:onCancel");
            LoginManager.getInstance().logOut();
            //Toast.makeText(LoginActivity.this, "Login cancelled", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException error) {
            Log.d(TAG, "facebook:onError", error);
            Toast.makeText(LoginActivity.this, "Login Error", Toast.LENGTH_LONG).show();
        }
    };

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(LoginActivity.this, String.valueOf(resultCode),
        //       Toast.LENGTH_SHORT).show();

        if (requestCode == RC_SIGN_IN) {
            if(resultCode != 0){
                ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Processing");
                progressDialog.show();
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    GoogleSignInAccount account = result.getSignInAccount();
                    assert account != null;
                    String name = account.getDisplayName();
                    String email = account.getEmail();
                    String token = account.getIdToken();
                    String personPhoto = ((account.getPhotoUrl() != null) ? account.getPhotoUrl().toString() : null);

                    prefManager.setUserLoggedIn(true, name, email, token, personPhoto);
                    progressDialog.dismiss();
                    Toast.makeText(this, "Welcome back " + name + "!", Toast.LENGTH_SHORT).show();
                    loginStatus();
                }
            }
        } else if (requestCode == RC_FB_IN){
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else {
            Toast.makeText(LoginActivity.this, "Error in Sigining", Toast.LENGTH_LONG).show();
        }
    }

    public void authSignOut() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.login_button:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                LoginManager.getInstance().registerCallback(callbackManager, mFacebookCallback);
                break;
            case R.id.activity_close:
                loginStatus();
                break;
            case R.id.login_btn:
                userLogin();
                break;
            case R.id.register:
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                break;
            case R.id.skip:
                loginStatus();
                break;
        }
    }

    private void userLogin() {
        EditText user_name_edit = (EditText) findViewById(R.id.user_email);
        EditText user_pass_edit = (EditText) findViewById(R.id.user_pass);
        final String user_name = user_name_edit.getText().toString().trim();
        final String user_pass = user_pass_edit.getText().toString().trim();

        if(user_name.length() > 0) {
            if (user_pass.length() > 0) {
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Logging In..");
                progressDialog.show();

                gc.hideKeyboard(findViewById(android.R.id.content));

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(Constant.CONNECT_URL, Constant.LOGIN_URL);

                String url = gc.urlCreator(hashMap);

                StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @SuppressWarnings("unchecked")
                            @Override
                            public void onResponse(String response) {
                                Log.e(TAG, response);
                                if (response!= null) {
                                    updateLoginStatus(response);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(LoginActivity.this,
                                        "Some error occurred!", Toast.LENGTH_SHORT).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_login", user_name);
                        params.put("user_pass", user_pass);

                        return params;
                    }
                };

                jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                jsObjRequest.setShouldCache(false);
                AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
            } else {
                Toast.makeText(this, "Please enter Password!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please enter Username!", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateLoginStatus(String response) {
        JSONObject user;
        try {
            user = new JSONObject(response);
            if(user.getString("success").equals("true")){
                JSONObject data = user.getJSONObject("data");
                String name = data.getString("display_name");
                String email = data.getString("user_email");
                //String personPhoto = ((user.getPhotoUrl() != null) ? user.getPhotoUrl().toString() : null);

                prefManager.setUserLoggedIn(true, name, email, null, null);
                progressDialog.dismiss();
                Toast.makeText(this, "Welcome back " + name + "!", Toast.LENGTH_SHORT).show();
                loginStatus();
            } else {
                progressDialog.dismiss();
                Toast.makeText(this, user.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,
                "Error in Signing!", Toast.LENGTH_SHORT).show();
    }



    // private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
    //     Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

    //     AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
    //     mAuth.signInWithCredential(credential)
    //             .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
    //                 @Override
    //                 public void onComplete(@NonNull Task<AuthResult> task) {
    //                     if (task.isSuccessful()) {
    //                         // Sign in success, update UI with the signed-in user's information
    //                         Log.d(TAG, "signInWithCredential:success");
    //                         FirebaseUser user = mAuth.getCurrentUser();
    //                         updateUI(user);
    //                     } else {
    //                         // If sign in fails, display a message to the user.
    //                         Log.w(TAG, "signInWithCredential:failure", task.getException());
    //                         Toast.makeText(LoginActivity.this, "Authentication failed.",
    //                                 Toast.LENGTH_SHORT).show();
    //                         updateUI(null);
    //                     }

    //                     // ...
    //                 }
    //             });
    // }

    
}
