package com.codeondev.indianfreejobs.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.RecentPostAdapter;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class AuthorDetailsActivity extends AppCompatActivity{
    private String TAG = "AUTHOR_DETAILS";
    private ArrayList<RecentPost> myDataSet = new ArrayList<>();
    private GlobalClass gc;
    private RecentPostAdapter myAdapter;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authors_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressDialog = new ProgressDialog(AuthorDetailsActivity.this);
        progressDialog.setMessage("Loading Profile..");
        progressDialog.show();

        myAdapter = new RecentPostAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.author_posts);
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);
        myRecyclerView.setNestedScrollingEnabled(false);

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                RecentPost recentPost = myDataSet.get(position);
                Intent intent = new Intent(AuthorDetailsActivity.this, PostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", recentPost.getId());
                //bundle.putString("title", recentPost.getTitle());
                //bundle.putString("guid", recentPost.getGuid());
                //bundle.putString("count", recentPost.getCmtCount());
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        updateData();

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateData();
    }

    private void updateData() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.AUTHORS_URL);
        hashMap.put(Constant.ID, String.valueOf(bundle.getInt("auth_id")));

        String url = gc.urlCreator(hashMap);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                if(response.toString() != null) {
                    myDataSet.clear();
                    showAuthor(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AuthorDetailsActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });

        jsObjRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);

    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    private void showAuthor(JSONObject response) {
        JSONObject author;
        JSONArray posts;
        try {
            author = response.getJSONObject("author");
            posts = response.getJSONArray("posts");
            for(int i=0 ; i< posts.length(); i++){
                JSONObject post;
                try {
                    post = posts.getJSONObject(i);
                    myDataSet.add(new RecentPost(
                            post.getInt("ID"),
                            gc.htmlStringParser(post.getString("post_title")),
                            gc.htmlStringParser(post.getString("image")),
                            post.getString("post_date_gmt"),
                            post.getString("guid"),
                            post.getString("comment_count"),
                            post.getString("post_view")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            String user_nm = "@" + author.getString("user_nicename");

            ((TextView) findViewById(R.id.author_name)).setText(author.getString("display_name"));
            ((TextView) findViewById(R.id.author_username)).setText(user_nm);
            ((TextView) findViewById(R.id.author_email)).setText(author.getString("user_email"));
            final ImageView auth_image = (ImageView) findViewById(R.id.author_image);

            Log.e(TAG, gc.htmlStringParser(author.getString("image")));
            Glide.with(getApplicationContext()).
                    load(gc.htmlStringParser(author.getString("image")))
                    .asBitmap()
                    .placeholder(R.drawable.user)
                    .centerCrop().into(new BitmapImageViewTarget(auth_image) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    auth_image.setImageDrawable(circularBitmapDrawable);
                }
            });

            myRecyclerView.setAdapter(myAdapter);
            myAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
            progressDialog.dismiss();
        }
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
