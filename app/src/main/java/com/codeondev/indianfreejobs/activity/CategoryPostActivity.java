package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.CategoryAdapter;
import com.codeondev.indianfreejobs.adapter.RecentPostAdapter;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.Category;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class CategoryPostActivity extends AppCompatActivity{
    private String TAG = "CATEGORY_POST";
    private RecentPostAdapter myAdapter;
    private ArrayList<RecentPost> myDataSet = new ArrayList<>();
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private GlobalClass gc;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CoordinatorLayout coordinateLayout;
    private String cat_name, cat_desc, cat_cnt;
    private String cat_id;
    private String cat_nm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categorypost_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        cat_id = bundle.getString("cat_id");
        cat_nm = bundle.getString("cat_name");

        toolbar.setTitle(gc.htmlStringParser(cat_nm));
        setSupportActionBar(toolbar);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);

        myAdapter = new RecentPostAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.categorypost_listview);

        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);
        myRecyclerView.setNestedScrollingEnabled(false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                        updateList();
                    }
                }
        );

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                RecentPost recentPost = myDataSet.get(position);
                Intent intent = new Intent(CategoryPostActivity.this, PostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", recentPost.getId());
                //bundle.putString("guid", recentPost.getGuid());
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    private void updateList() {
        if(gc.checkNetworkStatus()) {


            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Constant.CONNECT_URL, Constant.ALL_CATEGORY_URL);
            hashMap.put(Constant.ID, cat_id);

            String url = gc.urlCreator(hashMap);

            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e(TAG, response.toString());
                    if (response.toString() != null) {
                        myDataSet.clear();
                        showCategory(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);

        } else {
                Snackbar snackbar = Snackbar
                        .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                        .setAction("CONNECT", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            }
                        });
                snackbar.setActionTextColor(Color.RED);
                snackbar.show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    private void showCategory(JSONObject response) {
        JSONObject cat_details;
        JSONArray posts;
        try {
            cat_details = response.getJSONObject("category");

            cat_name = gc.htmlStringParser(cat_details.getString("cat_name"));
            cat_desc = cat_details.getString("category_description");
            cat_cnt = cat_details.getString("category_count");

            posts = response.getJSONArray("posts");
            for (int i=0; i< posts.length(); i++) {
                JSONObject post;
                try {
                    post = posts.getJSONObject(i);
                    myDataSet.add(new RecentPost(
                            post.getInt("ID"),
                            gc.htmlStringParser(post.getString("post_title")),
                            gc.htmlStringParser(post.getString("image")),
                            post.getString("post_date_gmt"),
                            post.getString("guid"),
                            post.getString("comment_count"),
                            post.getString("post_view")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);

        ((TextView) findViewById(R.id.post_cnt)).setText(cat_cnt + " Posts");
        ((TextView) findViewById(R.id.cat_name)).setText(cat_name);
        if(!(cat_desc == null || cat_desc.equals(""))) {
            findViewById(R.id.cat_desc).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.cat_desc)).setText(cat_desc);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
