package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.CategoryAdapter;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.Category;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class CategoryActivity extends AppCompatActivity{
    private String TAG = "CATEGORY";
    private CategoryAdapter myAdapter;
    private ArrayList<Category> myDataSet = new ArrayList<>();;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private GlobalClass gc;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private CoordinatorLayout coordinateLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);

        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.custom_divider));

        myAdapter = new CategoryAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.category_listview);
        myRecyclerView.addItemDecoration(divider);
        myRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                        updateList();
                    }
                }
        );

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Category category = myDataSet.get(position);
                Intent intent = new Intent(CategoryActivity.this, CategoryPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("cat_id", category.getID());
                bundle.putString("cat_name", category.getTitle());
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    private void updateList() {
        if(gc.checkNetworkStatus()) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(Constant.CONNECT_URL, Constant.ALL_CATEGORY_URL);

            String url = gc.urlCreator(hashMap);

            JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e(TAG, response.toString());
                    if (response.toString() != null) {
                        myDataSet.clear();
                        showCategory(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);

        } else {
                Snackbar snackbar = Snackbar
                        .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                        .setAction("CONNECT", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                            }
                        });
                snackbar.setActionTextColor(Color.RED);
                snackbar.show();
                mSwipeRefreshLayout.setRefreshing(false);
            }
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    private void showCategory(JSONArray response) {
        for (int i=0; i< response.length(); i++) {
            JSONObject category;
            try {
                category = response.getJSONObject(i);
                myDataSet.add(new Category(
                        category.getString("cat_ID"),
                        category.getString("name"),
                        gc.htmlStringParser(category.getString("category_description")),
                        category.getString("count")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
