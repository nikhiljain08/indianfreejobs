package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;

/**
 * Created by Nikhil Jain on 10-Jan-16.
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

        GlobalClass gc = new GlobalClass(SplashActivity.this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        final PrefManager prefManager = new PrefManager(this);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run(){
                if(prefManager.isFirstTimeLaunch()) {
                    prefManager.setFirstTimeLaunch(false);
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SplashActivity.this, Home_New.class));
                    finish();
                }
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() { }
}
