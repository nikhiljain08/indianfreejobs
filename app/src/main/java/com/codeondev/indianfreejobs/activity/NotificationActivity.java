package com.codeondev.indianfreejobs.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.NotificationAdapter;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.NotificationDB;
import com.codeondev.indianfreejobs.util.GlobalClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikhil Jain on 06-Jun-17.
 */

public class NotificationActivity extends AppCompatActivity{
    private String TAG = "NOTIFICATION";
    private NotificationAdapter myAdapter;
    private ArrayList<NotificationDB> myDataSet = new ArrayList<>();;
    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private GlobalClass gc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        List<NotificationDB> notificationArrayList = new ArrayList<>();
        notificationArrayList.add(new NotificationDB("nikhil", "123", 0, "", ""));
        notificationArrayList.add(new NotificationDB("nj", "1", 0, "", ""));
        notificationArrayList.add(new NotificationDB("nik", "13", 0, "", ""));

        //NotificationDB.addDataList(notificationArrayList);

        myAdapter = new NotificationAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.notification_listview);
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                NotificationDB notification = myDataSet.get(position);
                notification.setStatus(1);
                Toast.makeText(getApplicationContext(), notification.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
                NotificationDB.updateReadStatus(notification);
                Log.e(TAG, String.valueOf(position));
                myDataSet.get(position).setStatus(1);
                myAdapter.notifyItemChanged(position);
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }

    private void updateList() {
        myDataSet.clear();
        myDataSet.addAll(NotificationDB.getAllData());
        myAdapter.notifyDataSetChanged();
        if(myDataSet.size() > 0){
            findViewById(R.id.no_noti_text).setVisibility(View.INVISIBLE);
            myRecyclerView.setVisibility(View.VISIBLE);
        } else if(myDataSet.size() == 0){
            findViewById(R.id.no_noti_text).setVisibility(View.VISIBLE);
            myRecyclerView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear) {
            NotificationDB.deleteAllData();
            updateList();
        }

        return super.onOptionsItemSelected(item);
    }
}
