package com.codeondev.indianfreejobs.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.codeondev.indianfreejobs.AppController;
import com.codeondev.indianfreejobs.ImageSliderFragment;
import com.codeondev.indianfreejobs.R;
import com.codeondev.indianfreejobs.adapter.RecentPostAdapter;
import com.codeondev.indianfreejobs.database.DBAdapter;
import com.codeondev.indianfreejobs.helper.EndlessParentScrollListener;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.DeviceInfo;
import com.codeondev.indianfreejobs.util.GlobalClass;
import com.codeondev.indianfreejobs.util.PrefManager;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Home extends AppCompatActivity
        implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<RecentPost> myDataSet = new ArrayList<>();
    private RecentPostAdapter myAdapter;
    private String TAG = "HOME";
    private GlobalClass gc;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ViewPager mImageViewPager;
    private NestedScrollView scrollView;

    private Handler handler;
    private int delay = 4000; //milliseconds
    private int page = 0;
    private MyPagerAdapter myPagerAdapter;
    private CoordinatorLayout coordinateLayout;
    private int NUM_ITEMS = 4;
    private ImageView[] dots;
    private LinearLayout linearLayout_dots;
    private long back_pressed;
    private PrefManager prefManager;
    private boolean isLoading = false;
    private View header;
    private int loadedItemCount = 0, serverItemsCount = 0, offset = 0;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handler = new Handler();

        gc = new GlobalClass(this);
        View v = findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        DBAdapter db = new DBAdapter();
        db.init(this);

        prefManager = new PrefManager(this);

        final String token = FirebaseInstanceId.getInstance().getToken();
        //Log.e(TAG, token);

        if (!prefManager.getFCMToken().equals(token) && token != null) {
            updateFCMtoken(token);
        }

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);
        scrollView = (NestedScrollView) findViewById(R.id.scroll);
        pb = (ProgressBar) findViewById(R.id.loadmore_pb);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header = navigationView.getHeaderView(0);
        header.findViewById(R.id.profile_header_layout).setOnClickListener(this);

        Menu m = navigationView.getMenu();
        Typeface typeface = Typeface.createFromAsset(getApplicationContext().getAssets(),
                Constant.FONT_NAME_R);

        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            SpannableString s1 = new SpannableString(mi.getTitle());
            s1.setSpan(new TypefaceSpan(typeface), 0, s1.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mi.setTitle(s1);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    SpannableString s = new SpannableString(subMenuItem.getTitle());
                    s.setSpan(new TypefaceSpan(typeface), 0, s.length(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    subMenuItem.setTitle(s);
                }
            }
        }

        mImageViewPager = (ViewPager) findViewById(R.id.image_slider);

        drawPageSelectionIndicators(0);
        mImageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                drawPageSelectionIndicators(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        myAdapter = new RecentPostAdapter(this, myDataSet);
        myRecyclerView = (RecyclerView) findViewById(R.id.main_listview);
        mLinearLayoutManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);
        myRecyclerView.setNestedScrollingEnabled(false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                        myDataSet.clear();
                        offset = 0;
                        loadedItemCount = 0;
                        serverItemsCount = 0;
                        updateList();
                    }
                }
        );

        scrollView.setOnScrollChangeListener(new EndlessParentScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.e(TAG, String.valueOf(page));
                if (loadedItemCount <= serverItemsCount) {
                    updateList();
                }
            }
        });

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (myDataSet.size() > 0) {
                    RecentPost recentPost = myDataSet.get(position);
                    Intent intent = new Intent(Home.this, PostActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", recentPost.getId());
                    //bundle.putString("title", recentPost.getTitle());
                    //bundle.putString("guid", recentPost.getGuid());
                    //bundle.putString("count", recentPost.getCmtCount());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

    }

    private void updateFCMtoken(final String token) {
        final DeviceInfo info = new DeviceInfo(getApplicationContext());

        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put(Constant.CONNECT_URL, Constant.FCM_TOKEN_REG);

        String url = gc.urlCreator(hashMap);
        Log.e(TAG, url);
        StringRequest jsObjRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, response);
                        if (response!= null) {
                            prefManager.setFCMToken(token);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Home.this,
                                "Some error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constant.DEVICE_ID, info.getAndroidID());
                params.put(Constant.DEVICE_TYPE, "Android");
                params.put(Constant.FCM_TOKEN, token);
                if(prefManager.userEmail().equals(""))
                    params.put("email", prefManager.userEmail());
                params.put("subscribe", "global");

                return params;
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsObjRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(jsObjRequest,TAG);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateProfile();
        updateList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    public void updateProfile(){
        if (header instanceof ViewGroup)
            gc.setFont(header);

        if(prefManager.isUserLogged()){

            header.findViewById(R.id.slider_name).setVisibility(View.VISIBLE);
            ((TextView) header.findViewById(R.id.slider_name)).setText(prefManager.userName());

            header.findViewById(R.id.slider_email).setVisibility(View.VISIBLE);;
            ((TextView) header.findViewById(R.id.slider_email)).setText(prefManager.userEmail());

            final ImageView img = (ImageView) header.findViewById(R.id.slider_image);
            img.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).
                    load(prefManager.userProfile())
                    .asBitmap()
                    .placeholder(R.drawable.user)
                    .centerCrop().into(new BitmapImageViewTarget(img) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    img.setImageDrawable(circularBitmapDrawable);
                }
            });
        } else {
            header.findViewById(R.id.no_login).setVisibility(View.VISIBLE);
        }
    }

    private void updateList(){
        if(gc.checkNetworkStatus()) {
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put(Constant.CONNECT_URL, Constant.RECENT_POSTS_URL);
            hashMap.put(Constant.COUNT, String.valueOf(Constant.LOAD_LIMIT));
            hashMap.put(Constant.OFFSET, String.valueOf(offset));

            String url = gc.urlCreator(hashMap);

            JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e(TAG, response.toString());
                    if(response.toString() != null) {
                        scrollView.setVisibility(View.VISIBLE);
                        showRecentPost(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
        isLoading = false;
    }

    private void showRecentPost(JSONArray response) {
        loadedItemCount = serverItemsCount;
        for (int i=0; i< response.length(); i++) {
            JSONObject recentPost;
            try {
                recentPost = response.getJSONObject(i);
                myDataSet.add(new RecentPost(
                        recentPost.getInt("ID"),
                        gc.htmlStringParser(recentPost.getString("post_title")),
                        gc.htmlStringParser(recentPost.getString("image")),
                        recentPost.getString("post_date_gmt"),
                        recentPost.getString("guid"),
                        recentPost.getString("comment_count"),
                        recentPost.getString("post_view")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        offset += Constant.LOAD_LIMIT;
        serverItemsCount = response.length();
        if(serverItemsCount == 0)
            pb.setVisibility(View.GONE);
        else
            pb.setVisibility(View.VISIBLE);

        if (myDataSet.size() > 0){
            myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            mImageViewPager.setAdapter(myPagerAdapter);
        }

        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()){
                super.onBackPressed();
            }
            else{
                Snackbar.make(findViewById(R.id.coordinatelayout),
                        "Press once again to exit!", Snackbar.LENGTH_LONG).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            startActivity(new Intent(Home.this, SearchViewActivity.class));
        } else if (id == R.id.action_notification) {
            startActivity(new Intent(Home.this, NotificationActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.nav_category) {
            startActivity(new Intent(this, CategoryActivity.class));
        } else if (id == R.id.nav_author) {
            startActivity(new Intent(this, AuthorActivity.class));
        } else if (id == R.id.nav_post) {

        } else if (id == R.id.nav_page) {
            startActivity(new Intent(this, PagesActivity.class));

        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutUs.class));

        } else if (id == R.id.nav_contact) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_header_layout:
                if(!prefManager.isUserLogged())
                    startActivity(new Intent(Home.this, LoginActivity.class));
                break;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        private MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ImageSliderFragment.newInstance(myDataSet.get(0).getId(),
                            myDataSet.get(0).getImage(), myDataSet.get(0).getTitle(),
                            myDataSet.get(0).getGuid(), myDataSet.get(0).getCmtCount());
                case 1:
                    return ImageSliderFragment.newInstance(myDataSet.get(1).getId(),
                            myDataSet.get(1).getImage(), myDataSet.get(1).getTitle(),
                            myDataSet.get(1).getGuid(), myDataSet.get(1).getCmtCount());
                case 2:
                    return ImageSliderFragment.newInstance(myDataSet.get(2).getId(),
                            myDataSet.get(2).getImage(), myDataSet.get(2).getTitle(),
                            myDataSet.get(2).getGuid(), myDataSet.get(2).getCmtCount());
                case 3:
                    return ImageSliderFragment.newInstance(myDataSet.get(3).getId(),
                            myDataSet.get(3).getImage(), myDataSet.get(3).getTitle(),
                            myDataSet.get(3).getGuid(), myDataSet.get(3).getCmtCount());
                default:
                    return null;
            }
        }
    }

    Runnable runnable = new Runnable() {
        public void run() {
            if (myPagerAdapter!=null){
                if (myPagerAdapter.getCount() == page)
                    page = 0;
                else
                    page++;

                mImageViewPager.setCurrentItem(page, true);
                handler.postDelayed(this, delay);
            }
        }
    };

    private class TypefaceSpan extends MetricAffectingSpan {
        private final Typeface typeface;

        public TypefaceSpan(Typeface typeface) {
            this.typeface = typeface;
        }

        private void apply(Paint paint, Typeface typeface) {
            paint.setTypeface(typeface);
        }

        @Override
        public void updateDrawState(TextPaint textPaint) {
            apply(textPaint, typeface);
        }

        @Override
        public void updateMeasureState(TextPaint textPaint) {
            apply(textPaint, typeface);
        }
    }
    
    private void drawPageSelectionIndicators(int mPosition){
        if(linearLayout_dots != null) {
            linearLayout_dots.removeAllViews();
        }
        
        linearLayout_dots = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        dots = new ImageView[NUM_ITEMS];
        
        for (int i = 0; i < NUM_ITEMS; i++) {
            dots[i] = new ImageView(this);
            if(i == mPosition)
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.item_selected));
            else
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.item_unselected));
    
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
    
            params.setMargins(4, 0, 4, 0);
            linearLayout_dots.addView(dots[i], params);
        }
    }
}
