package com.codeondev.indianfreejobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codeondev.indianfreejobs.activity.PostActivity;
import com.codeondev.indianfreejobs.util.GlobalClass;

/**
 * Created by nikhil on 11/6/17.
 */

public class ImageSliderFragment extends Fragment {

    private int id;
    private String title_txt, image_url, guid, count;
    private GlobalClass gc;

    public static ImageSliderFragment newInstance(int ID, String image_url, String title,
                                                  String guid, String count) {
        ImageSliderFragment imageSliderFragment = new ImageSliderFragment();
        Bundle args = new Bundle();
        args.putInt("ID", ID);
        args.putString("image", image_url);
        args.putString("title", title);
        args.putString("guid", guid);
        args.putString("count", count);
        imageSliderFragment.setArguments(args);
        return imageSliderFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gc = new GlobalClass(getActivity());
        id = getArguments().getInt("ID", 0);
        image_url = getArguments().getString("image", "R.mipmap.ic_launcher");
        title_txt = getArguments().getString("title");
        guid = getArguments().getString("guid");
        count = getArguments().getString("count");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pager_item, container, false);

        View v = view.findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        ImageView image = (ImageView) view.findViewById(R.id.img_pager_item);
        final TextView title = (TextView) view.findViewById(R.id.title_pager_item);

        title.setText(title_txt);
        Glide.with(getActivity())
                .load(image_url)
                .into(image);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),"Clicked",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), PostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", id);
                bundle.putString("title", title_txt);
                bundle.putString("guid", guid);
                bundle.putString("count", count);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        return view;
    }
}
