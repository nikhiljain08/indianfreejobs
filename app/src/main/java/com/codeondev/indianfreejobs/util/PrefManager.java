package com.codeondev.indianfreejobs.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Nikhil Jain on 02-Oct-16.
 */

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "codeondev-indianfreejobs";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_USER_LOGGED = "IsUserLoggedIn";
    private static final String IS_NOTIFICATION_ON = "IsNotificationOn";
    private static final String USER_NAME = "name";
    private static final String USER_EMAIL = "email";
    private static final String USER_TOKEN = "token";
    private static final String USER_PRO_PIC = "profile_pic";
    private static final String FCM_TOKEN = "fcm_token";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);

    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor = pref.edit();
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.apply();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setNotification(boolean isNotificationOn) {
        editor = pref.edit();
        editor.putBoolean(IS_NOTIFICATION_ON, isNotificationOn);
        editor.apply();
    }

    public boolean isNotificationOn() {
        return pref.getBoolean(IS_NOTIFICATION_ON, true);
    }

    public void setUserLoggedIn(boolean userLogged, String name, String email, String token, String image) {
        editor = pref.edit();
        editor.putBoolean(IS_USER_LOGGED, userLogged);
        editor.putString(USER_NAME, name);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_TOKEN, token);
        editor.putString(USER_PRO_PIC, image);
        editor.apply();
    }

    public boolean isUserLogged() {
        return pref.getBoolean(IS_USER_LOGGED, false);
    }
    public String userName() {
        return pref.getString(USER_NAME, null);
    }
    public String userEmail() {
        return pref.getString(USER_EMAIL, "");
    }
    public String userProfile() {
        return pref.getString(USER_PRO_PIC, null);
    }
    public String userToken() {
        return pref.getString(USER_TOKEN, null);
    }

    public String getFCMToken(){
        return pref.getString(FCM_TOKEN, "");
    }

    public void setFCMToken(String token){
        editor = pref.edit();
        editor.putString(FCM_TOKEN, token);
        editor.apply();
    }
}
