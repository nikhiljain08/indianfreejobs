package com.codeondev.indianfreejobs.util;

/**
 * Created by Nikhil Jain on 07-Jun-17.
 */

public class Constant {
    public final static String FONT_NAME_R = "fonts/OpenSans-Regular.ttf";
    public final static String FONT_NAME_B = "fonts/OpenSans-Bold.ttf";
    public final static String FONT_NAME_I = "fonts/OpenSans-Italic.ttf";
    public final static String FONT_NAME_BI = "fonts/OpenSans-BoldItalic.ttf";

    public final static String MAIN_URL = "www.indianfreejobs.com";
    //public final static String MAIN_URL = "www.maalgudirails.com";
    public final static String CONNECT_URL = "jakhar_api";
    public final static String ID = "ID";
    public final static String QUERY = "query";
    public final static String ALL_CATEGORY_URL = "post_categories";
    public final static String RECENT_POSTS_URL = "post_recent";
    public final static String TRENDING_POSTS_URL = "trending";
    public final static String TOP10_POSTS_URL = "top10";
    public final static String POST_URL = "post_single";
    public final static String AUTHORS_URL = "post_authors";
    public final static String PAGES_URL = "pages";
    public final static String ADD_COMMENT_URL = "comment_post";
    public final static String SEARCH_URL = "search";
    public final static String LOGIN_URL = "login";
    public static final String SIGNUP_URL = "signup";
    public static final String COUNT = "count";
    public static final String OFFSET = "offset";
    public static final int LOAD_LIMIT = 10;
    //public final static String POST_URL = "/wp-json/wp/v2/posts/260";


    //DATABASE CONSTANTS
    public final static String NOTIFICATION_TBL = "notification";
    public final static String NOTIFICATION_TBL_CREATE =
            "create table " + NOTIFICATION_TBL + " ( " +
                    "_id integer primary key autoincrement, " +
                    "noti_title text not null, " +
                    "noti_msg text not null, " +
                    "noti_read_status int NOT NULL CHECK (noti_read_status IN (0,1)), " +
                    "noti_img_url text, " +
                    "noti_dttm datetime);";


    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
    public static final String FCM_TOKEN_REG = "device";
    public static final String DEVICE_ID = "device_id";
    public static final String DEVICE_TYPE = "device_type";
    public static final String FCM_TOKEN = "token";
}
