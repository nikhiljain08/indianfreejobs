package com.codeondev.indianfreejobs.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeondev.indianfreejobs.R;

public class GlobalProgressBar {
    Dialog dialog;
    Boolean check = false;
    Context context;
    TextView title_textview;
    ProgressBar progBar;

    public GlobalProgressBar(Context context) {
        this.context = context;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loader);
        title_textview = (TextView) dialog.findViewById(R.id.loader_title);
        progBar = (ProgressBar) dialog.findViewById(R.id.progressBar);
        if (progBar.getIndeterminateDrawable() != null) {
            progBar.getIndeterminateDrawable().setColorFilter(context.getResources().getColor(R.color.colorAccent),
                    PorterDuff.Mode.SRC_IN);
        }
    }

    public void show() {
        dialog.setCancelable(false);
        dialog.show();
        check = true;
    }

    public void dismiss() {
        dialog.dismiss();
        check = false;
    }

    public void setTitle(String title) {
        title_textview.setText(title);
    }

    public boolean isShowing() {
        return check;
    }
}
