package com.codeondev.indianfreejobs;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.codeondev.indianfreejobs.activity.PostActivity;
import com.codeondev.indianfreejobs.adapter.RecentPostAdapter;
import com.codeondev.indianfreejobs.helper.EndlessParentScrollListener;
import com.codeondev.indianfreejobs.helper.RecyclerTouchListener;
import com.codeondev.indianfreejobs.provider.RecentPost;
import com.codeondev.indianfreejobs.util.Constant;
import com.codeondev.indianfreejobs.util.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nikhil on 11/6/17.
 */

public class RecentPostFragment extends Fragment {

    private GlobalClass gc;
    private View view;

    private RecyclerView myRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<RecentPost> myDataSet = new ArrayList<>();
    private RecentPostAdapter myAdapter;

    private CoordinatorLayout coordinateLayout;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ViewPager mImageViewPager;
    private NestedScrollView scrollView;
    private Handler handler;
    private int delay = 4000; //milliseconds
    private int page = 0;
    private MyPagerAdapter myPagerAdapter;

    private int NUM_ITEMS = 4;

    private ImageView[] dots;
    private LinearLayout linearLayout_dots;

    private int loadedItemCount = 0, serverItemsCount = 0, offset = 0;
    private ProgressBar pb;

    private String TAG = "Recent Post Fragment";

    public static RecentPostFragment newInstance() {
        return new RecentPostFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gc = new GlobalClass(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recentpost_frag, container, false);

        coordinateLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatelayout);
        scrollView = (NestedScrollView) view.findViewById(R.id.scroll);
        pb = (ProgressBar) view.findViewById(R.id.loadmore_pb);

        View v = view.findViewById(android.R.id.content);
        if (v instanceof ViewGroup)
            gc.setFont(v);

        handler = new Handler();
        handler.postDelayed(runnable, delay);

        myPagerAdapter = new MyPagerAdapter(getChildFragmentManager());

        mImageViewPager = (ViewPager) view.findViewById(R.id.image_slider);
        mImageViewPager.setOffscreenPageLimit(4);

        drawPageSelectionIndicators(0);
        mImageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                drawPageSelectionIndicators(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        myRecyclerView = (RecyclerView) view.findViewById(R.id.main_listview);
        myAdapter = new RecentPostAdapter(getActivity(), myDataSet);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());

        myRecyclerView.setLayoutManager(mLinearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(myAdapter);
        myRecyclerView.setNestedScrollingEnabled(false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        mSwipeRefreshLayout.setRefreshing(true);

        mSwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
                        myDataSet.clear();
                        offset = 0;
                        loadedItemCount = 0;
                        serverItemsCount = 0;
                        updateList();
                    }
                }
        );

        scrollView.setOnScrollChangeListener(new EndlessParentScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.e(TAG, String.valueOf(page));
                if (loadedItemCount <= serverItemsCount) {
                    updateList();
                }
            }
        });

        myRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(),
                myRecyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (myDataSet.size() > 0) {
                    RecentPost recentPost = myDataSet.get(position);
                    Intent intent = new Intent(getActivity(), PostActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", recentPost.getId());
                    //bundle.putString("title", recentPost.getTitle());
                    //bundle.putString("guid", recentPost.getGuid());
                    //bundle.putString("count", recentPost.getCmtCount());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) { }
        }));

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateList();
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(runnable, delay);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onStop() {
        super.onStop();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    private void updateList(){
        if(gc.checkNetworkStatus()) {
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put(Constant.CONNECT_URL, Constant.RECENT_POSTS_URL);
            hashMap.put(Constant.COUNT, String.valueOf(Constant.LOAD_LIMIT));
            hashMap.put(Constant.OFFSET, String.valueOf(offset));

            String url = gc.urlCreator(hashMap);

            JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url,
                    null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e(TAG, response.toString());
                    if(response.toString() != null) {
                        scrollView.setVisibility(View.VISIBLE);
                        showRecentPost(response);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar snackbar = Snackbar
                            .make(coordinateLayout, "Connectivity Issue", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mSwipeRefreshLayout.setRefreshing(true);
                                    updateList();
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    snackbar.show();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });

            jsObjRequest.setShouldCache(false);
            AppController.getInstance().addToRequestQueue(jsObjRequest, TAG);
        } else {
            Snackbar snackbar = Snackbar
                    .make(coordinateLayout, "No Internet connection", Snackbar.LENGTH_INDEFINITE)
                    .setAction("CONNECT", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    });
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void showRecentPost(JSONArray response) {
        loadedItemCount = serverItemsCount;
        for (int i=0; i< response.length(); i++) {
            JSONObject recentPost;
            try {
                recentPost = response.getJSONObject(i);
                myDataSet.add(new RecentPost(
                        recentPost.getInt("ID"),
                        gc.htmlStringParser(recentPost.getString("post_title")),
                        gc.htmlStringParser(recentPost.getString("image")),
                        recentPost.getString("post_date_gmt"),
                        recentPost.getString("guid"),
                        recentPost.getString("comment_count"),
                        recentPost.getString("post_view")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        offset += Constant.LOAD_LIMIT;
        serverItemsCount = response.length();

        if (myDataSet.size() > 0){
            mImageViewPager.setAdapter(myPagerAdapter);
        }

        myRecyclerView.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);

        if(serverItemsCount < Constant.LOAD_LIMIT)
            pb.setVisibility(View.GONE);
        else
            pb.setVisibility(View.VISIBLE);
    }

    private void drawPageSelectionIndicators(int mPosition){
        if(linearLayout_dots != null) {
            linearLayout_dots.removeAllViews();
        }

        linearLayout_dots = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        dots = new ImageView[NUM_ITEMS];

        for (int i = 0; i < NUM_ITEMS; i++) {
            dots[i] = new ImageView(getContext());
            if(i == mPosition)
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.item_selected));
            else
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.item_unselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);
            linearLayout_dots.addView(dots[i], params);
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        private MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ImageSliderFragment.newInstance(myDataSet.get(0).getId(),
                            myDataSet.get(0).getImage(), myDataSet.get(0).getTitle(),
                            myDataSet.get(0).getGuid(), myDataSet.get(0).getCmtCount());
                case 1:
                    return ImageSliderFragment.newInstance(myDataSet.get(1).getId(),
                            myDataSet.get(1).getImage(), myDataSet.get(1).getTitle(),
                            myDataSet.get(1).getGuid(), myDataSet.get(1).getCmtCount());
                case 2:
                    return ImageSliderFragment.newInstance(myDataSet.get(2).getId(),
                            myDataSet.get(2).getImage(), myDataSet.get(2).getTitle(),
                            myDataSet.get(2).getGuid(), myDataSet.get(2).getCmtCount());
                case 3:
                    return ImageSliderFragment.newInstance(myDataSet.get(3).getId(),
                            myDataSet.get(3).getImage(), myDataSet.get(3).getTitle(),
                            myDataSet.get(3).getGuid(), myDataSet.get(3).getCmtCount());
                default:
                    return null;
            }
        }
    }

    Runnable runnable = new Runnable() {
        public void run() {
            if (myPagerAdapter!=null){
                if (myPagerAdapter.getCount() == page)
                    page = 0;
                else
                    page++;

                mImageViewPager.setCurrentItem(page, true);
                handler.postDelayed(this, delay);
            }
        }
    };

}
